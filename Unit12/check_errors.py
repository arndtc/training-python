#check_errors.py

from inputError import InputError
from denomError import DenomError

class check_error(object):
     def __init__(self, num1, num2):
          self.x = num1
          self.y = num2
          self.checkNegative()

     def divide(self):
         if self.y == 0:
             raise DenomError(self.y, "This class cannot allow y to be zero")
         return self.x // self.y

     def checkNegative(self):
          if self.x < 0:
               raise InputError(self.x, "This class requires the first number to be positive")
          if self.y < 0:
               raise InputError(self.y, "This class requires the second number to be positive")

num1 = int(raw_input("Please give me the first integer "))
num2 = int(raw_input("Please give me the second integer "))

a = check_error(num1, num2)
a.divide() 
