import re

class myWaitList():
    def __init__(self):
        self.aList = []
        self.noMatch = 1

    def PrintMenu(self):
        print ""
        print "Here are your options:"
        print "1: Print list"
        print "2: Add persion to the list"
        print "3: Remove a person from the list"
        print "4: Check to see if a person is in the list"
        print "5: Exit"
        print "6: Find names in list beginning with specified letter"
        print "7: Find names in list ending with specified letter"
        print "8: Find name of specified length"
        print "\n"

    def PrintList(self):
        print "Waiting list:\n"
        print "=============\n"
        print self.aList

    def PrintAdd(self,addName):
        self.aList.append(addName)
        print self.aList

    def RemoveName(self,removeName):
        for x in range(len(self.aList)):
            if self.aList[x] == removeName:
                del self.aList[x]
                break
        print self.aList

    def CheckName(self,checkName):
        for x in range(len(self.aList)):
            if self.aList[x] == checkName:
                print "The list does contain", self.aList[x]
                noMatch = 0;
                break
            else:
                noMatch = 1;
        if noMatch == 1:
            print "The list does not contain", checkName

    def FindStartLetterNames(self,pattern):
        ''' need to use re object and use match'''
        FirstLetterObject = re.compile(pattern)
        aFlag = False
        for x in range(len(self.aList)):
            if FirstLetterObject.search(self.aList[x]):
                aFlag = True
                print "You found this person in the list: " , self.aList[x]
        if aFlag == False:
            print "no one in the list fit your criteria."
        
    def FindEndLetterNames(self,pattern):
        ''' need to use re object with match'''
        LastLetterObject = re.compile(pattern + "$")
        aFlag = False
        for x in range(len(self.aList)):
            if LastLetterObject.search(self.aList[x]):
                aFlag = True
                print "You found this person in the list: " , self.aList[x]
        if aFlag == False:
            print "no one in the list fit your criteria."

    def FindCountLettersNames(self,count):
        ''' need to use re object with match'''
        CountLetterObject = re.compile('^[A-Za-z]{' + count + '}$')
        aFlag = False
        for x in range(len(self.aList)):
            if CountLetterObject.search(self.aList[x]):
               print "You found this person in the list: " , self.aList[x]
               aFlag = True
        if aFlag == False:
            print "no one in the list fit your criteria."

            
    def main(self):
        self.PrintMenu()
        option = raw_input("What would you like to do? ")

        while option != '5':
            if option == '1':
                self.PrintList()
                self.PrintMenu()
                option = raw_input("What would you like to do? ")
                continue
            elif option == '2':
                addName = raw_input("Enter name you wish to add: ")
                if not addName:
                    addName = raw_input("Enter name you wish to add: ")
                else:
                    self.PrintAdd(addName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '3':
                removeName = raw_input("Please enter name you with to remove: ")
                if not removeName:
                    removeName = raw_input("Please enter name you with to remove: ")
                else:
                    self.RemoveName(removeName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '4':
                checkName = raw_input("Please enter name you with to check: ")
                if not checkName:
                    raw_input("Please enter name you with to check: ")
                else:
                    self.CheckName(checkName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '5':
                print "Goodbye!"
            elif option == '6':
                findFirst = raw_input("Please enter the first letter of the names you want to find: ")
                print 'Searching for names that begin with: ' , findFirst
                if not findFirst:
                    findFirst = raw_input( "Please enter the first letter of the name you want to find:")
                else:
                    self.FindStartLetterNames(findFirst)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '7':
                findLast = raw_input("Please enter the LAST letter of the names you want to find: ")
                print 'You are searching for names that end with: ' , findLast
                if not findLast:
                    findLast = raw_input( "Please enter the LAST letter of the name you want to find: ")
                else:
                    self.FindEndLetterNames(findLast)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue                            
            elif option == '8':
                findLength = raw_input("Please enter the LENGTH of the names you want to find: ")
                print 'You are searching for names that end with: ' , findLength
                if not findLength:
                    findLength = raw_input( "Please enter the LENGTH of the names you want to find: ")
                else:
                    self.FindCountLettersNames(findLength)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue                            
            else:
                print "Goodbye!"
                break
