num = int(raw_input('Please enter your age: '))

while (num <= 0) or (num > 110):
    num = int(raw_input('Please enter a valid age: '))

if (num > 0) and (num < 16):
    print "You are not old enough to have any real fun."
elif (num >= 16) and (num < 21):
    print "You are old enough to drive."
elif (num >= 21) and (num < 55):
    print "You are old enough to drink."
else:
    print "You are old enough to enjoy the fruits of your labor."
