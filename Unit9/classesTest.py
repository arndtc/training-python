from classes import myWaitList

print("Do you want to run a program that will allow you to create a list and maintain it?")
runIt = raw_input('Y or N?')

if runIt == 'Y' or runIt == 'y':
    a = myWaitList()
    a.main()

else:
    print "Okay.  Maybe next time."
    print "Goodbye!"
