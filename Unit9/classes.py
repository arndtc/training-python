class myWaitList():
    def __init__(self):
        self.aList = []
        self.noMatch = 1

    def PrintMenu(self):
        print ""
        print "Here are your options:"
        print "1: Print list"
        print "2: Add persion to the list"
        print "3: Remove a person from the list"
        print "4: Check to see if a person is in the list"
        print "5: Exit\n"


    def PrintList(self):
        print "Waiting list:\n"
        print "=============\n"
        print self.aList

    def PrintAdd(self,addName):
        self.aList.append(addName)
        print self.aList

    def RemoveName(self,removeName):
        for x in range(len(self.aList)):
            if self.aList[x] == removeName:
                del self.aList[x]
                break
        print self.aList

    def CheckName(self,checkName):
        for x in range(len(self.aList)):
            if self.aList[x] == checkName:
                print "The list does contain", self.aList[x]
                noMatch = 0;
                break
            else:
                noMatch = 1;
        if noMatch == 1:
            print "The list does not contain", checkName
        
    def main(self):
        self.PrintMenu()
        option = raw_input("What would you like to do? ")

        while option != '5':
            if option == '1':
                self.PrintList()
                self.PrintMenu()
                option = raw_input("What would you like to do? ")
                continue
            elif option == '2':
                addName = raw_input("Enter name you wish to add: ")
                if not addName:
                    addName = raw_input("Enter name you wish to add: ")
                else:
                    self.PrintAdd(addName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '3':
                removeName = raw_input("Please enter name you with to remove: ")
                if not removeName:
                    removeName = raw_input("Please enter name you with to remove: ")
                else:
                    self.RemoveName(removeName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '4':
                checkName = raw_input("Please enter name you with to check: ")
                if not checkName:
                    raw_input("Please enter name you with to check: ")
                else:
                    self.CheckName(checkName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            else:
                break
