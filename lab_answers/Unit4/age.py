# Testing Control Flow
'''
Create a program called age.py that does the following:
#1.  Asks the user to input their age.
#2.  If the user gives an age of less than 1 or greater than 110, ask the user to provide  
a valid age.
#3.  If the user gives an age between 1 and 15, inform the user that they aren't old  
enough to have any real fun.
#4.  If the user gives an age between 16 and 20, inform the user that they are old enough  
to drive.
#5.  If the user gives an age between 21 and 55, inform the user that they are old enough  
to drink.
#5.  If the user gives an age between 55 and 110, inform the user that they are old  
enough to enjoy the fruit
'''
#use the raw_input function to give a prompt to users and take in what the user enters
age = 1

while age != 0:
    age = int(raw_input("Please provide a valid age or 0 to quit."))
    if age == 0:
        print "Goodbye!"
        break
    elif age < 1:
        print "Please don't make me laugh! How can you be a baby?"
        ageTest= raw_input( "Do you want to try again? y or n?")
        if ageTest == 'n':
            print "Goodbye!"
            break
        else:
            continue
    
    if age > 1 and age < 15:
        print "Too bad. You aren't old enough to have any real fun!"
        ageTest= raw_input( "Do you want to try again? y or n?")
        if ageTest == 'n':
            print "Goodbye!"
            break
        else:
            continue
    elif age > 15 and age < 20:
        print "Great. You are old enough to drive."
        ageTest= raw_input( "Do you want to try again? y or n?")
        if ageTest == 'n':
            print "Goodbye!"
            break
        else:
            continue
    elif age > 21 and age < 55:
        print "Great. You are old enough to drink."
        ageTest= raw_input( "Do you want to try again? y or n?")
        if ageTest == 'n':
            print "Goodbye!"
            break
        else:
            continue
    elif age > 55 and age < 111:
        print "Great. You are old enough to enjoy the fruits of your labor."
        ageTest= raw_input( "Do you want to try again? y or n?")
        if ageTest == 'n':
            print "Goodbye!"
            break
        else:
            continue
    elif age > 110:
        print "Please don't make me laugh! No one is THAT old."
        ageTest= raw_input( "Do you want to try again? y or n?")
        if ageTest == 'n':
            print "Goodbye!"
            break
        else:
            continue
