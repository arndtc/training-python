# files Test program
'''Return to the classes.py and classesTest.py programs.
Add functions to this class to:
write out the wait list to a new or existing file (can have option to overwrite).
read in a file of names and add them to a list.
open and search a file of names to add specified ones to the list. 
Add code  to test these new functions.
'''

from files import myListWaits

print("Do you want to run a program that allows you to create and maintain a list?")
runIt = raw_input('Y or N?')

if runIt == 'Y' or runIt == 'y':
    M = myListWaits()
    M.MenuProcess()
else:
    print "OK. Well maybe you wanted a brownie?!! Well we don't have one here."
    print "Goodbye!"
    
