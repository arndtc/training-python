# Regex Lab Example
'''
---conditional expressions Lab is waitlist.py
Create a program called waitlist.py that will allow someone to keep track of a wait list.
The program should have the following options provided to the user running the program in a menu:
#1.  Print the list
#2.  Add a person to the list
#3.  Remove a person from the list
#4.  Check to see if a user is in the list
#5.  Exit the program

---functions.py
Take the file that you created in the Conditional Expressions lab (waitlist.py)
and create functions for each of the menu options.
Make the list variable global throughout your program and all other variables
local. Names the functions module functions.py.

---modules.py
Create a file called modules.py. Copy the functions in functions.py into it. 
Now create another file called modulesTest.py and import the functions and test
them in a simple program.

---classes.py 
Return to your modules.py program and its modulesTest.py program.
Now convert the functions in modules.py into a class. Make the list and
menu global variables into class wide attributes. The functions become methods.
Test this class to make sure the menu and its options still work correctly.
Name the class program classes.py and the program that tests the class,
classesTest.py.

--regexpclasses.py
Return to classes.py which is now a class containing the menu functions.
Add functions to this class to search the list using wildcards such as:
all names beginning with a given letter
all names ending with a given letter
all names of a certain length
Add code to test these new functions.
'''
import re

class myListWaits():
    def __init__(self):
        
        self.aList = []
        self.Menu1 = ''' Thank you for joining our Root Beer Club.
               Your order is important to us, and we will
               take your order when we come to your place
               in line.  '''

        self.Menu2 = '''
               While waiting you have these options:
               =================================================================
               = Type 1 to print the entire list of people waiting  
               = Type 2 to add a name to our list.                  
               = Type 3 to remove yourself from the list.
               = Type 4 to check if a given person is waiting.
               = Type 5 to exit.
               = Type 6 to find names in the list beginning with a given letter.
               = Type 7 to find names in the list ending with a given letter.
               = Type 8 to find names of a certain length
               =================================================================
               '''
        
    def ChosePrint(self):
        print "=======================================================\n"
        print "This is the entire list of people waiting for Root Beer. \n", self.aList
        print "=======================================================\n"
        print 
        print self.Menu2
        
    def AddNewName(self,name):
        self.aList.append(name)
        print "=======================================================\n"
        print "This is the entire list now that you have added ", name
        print self.aList
        print "=======================================================\n"
        print self.Menu2
       
    def RemoveName(self,name):
        for x in range(len(self.aList)):
            if self.aList[x] == name:
                del self.aList[x] 
        print self.aList
        print self.Menu2

    def CheckName(self,name):
        for x in range(len(self.aList)):
            if self.aList[x] == name:
                print "You are checking for " , self.aList[x] 
                print "Here is the entire list \n", self.aList
        print self.Menu2

    def FindStartLetterNames(self,pattern):
        ''' need to use re object and use match'''
        FirstLetterObject= re.compile(pattern)
        aFlag = False
        for x in range(len(self.aList)):
            if FirstLetterObject.search(self.aList[x]):
                aFlag = True
                print "You found this person in the list: " , self.aList[x]
        if aFlag == False:
            print "no one in the list fit your criteria."
        print self.Menu2
        
    def FindEndLetterNames(self,pattern):
        ''' need to use re object with match'''
        LastLetterObject= re.compile(pattern + "$")
        aFlag = False
        for x in range(len(self.aList)):
            if LastLetterObject.search(self.aList[x]):
                aFlag = True
                print "You found this person in the list: " , self.aList[x]
        if aFlag == False:
            print "no one in the list fit your criteria."
        print self.Menu2


    def FindCountLettersNames(self,count):
        ''' need to use re object with match'''
        for x in range(len(self.aList)):
            if pattern.match(self.aList[x]):
               print "You found this person in the list: " , self.aList[x]
        print self.Menu2

               
             
    def MenuProcess(self):
        print self.Menu1
        print self.Menu2
        choice = raw_input("What would you like to do? " )
        while choice != '5':
            if choice == '1':
                self.ChosePrint()
                choice = raw_input("What would you like to do? " )
                continue
            elif choice == '2':
                AddOne = raw_input("Please enter the name you wish to add ")
                if not AddOne:
                    AddOne = raw_input( "Do you want to quit or add a person? Press 5 to quit. ")
                    if AddOne == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.AddNewName(AddOne)
                        choice = raw_input("What would you like to do? " )
                        continue
                else:
                    self.AddNewName(AddOne)
                    choice = raw_input("What would you like to do? " )
                    continue
            elif choice == '3':
                RemOne = raw_input("Please enter the name you wish to remove ")
                if not RemOne:
                    RemOne = raw_input( "Please give the name of the person you wish to remove or 5 to quit. ")
                    if RemOne == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.RemoveName(RemOne)
                    choice = raw_input("What would you like to do? " )
                    continue        
                else:
                    self.RemoveName(RemOne)
                    choice = raw_input("What would you like to do? " )
                    continue        
            elif choice == '4':
                ChkOne = raw_input("Please enter the name you wish to check for ")
                if not ChkOne:
                    ChkOne = raw_input( "Please give the name of the person you wish to check for or 5 to quit. ")
                    if ChkOne == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.CheckName(ChkOne)
                        choice = raw_input("What would you like to do? " )
                        continue        
                else:
                    self.CheckName(ChkOne)
                    choice = raw_input("What would you like to do? " )
                    continue        
            elif choice == '5':
                print "Good bye!"
            elif choice == '6':
                FindFirst = raw_input("Please enter the first letter of the names you are searching for. ")
                print 'You are searching for names that begin with: ' , FindFirst
                if not FindFirst:
                    FindFirst = raw_input( "Please enter the first letter of the name, for or 5 to quit. ")
                    if FindFirst == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.FindStartLetterNames(FindFirst)
                        choice = raw_input("What would you like to do? " )
                        continue        
                else:
                    self.FindStartLetterNames(FindFirst)
                    choice = raw_input("What would you like to do? " )
                    continue
            elif choice == '7':
                FindLast = raw_input("Please enter the LAST letter of the names you are searching for. ")
                print 'You are searching for names that end with: ' , FindLast
                if not FindLast:
                    FindLast = raw_input( "Please enter the LAST letter of the name, for or 5 to quit. ")
                    if FindLast == '5':
                        print "Goodbye!"
                        break
                    else:
                        self.FindEndLetterNames(FindLast)
                        choice = raw_input("What would you like to do? " )
                        continue        
                else:
                    self.FindEndLetterNames(FindLast)
                    choice = raw_input("What would you like to do? " )
                    continue            


            else:
                print "=========================================="
                print "= You did not choose 1 - 8!!!            ="
                print "= You will now exit the program          ="
                print "= Goodbye!                               ="
                print "=========================================="
                break


                           
                        
                           
                           
                           
                           

    
    

 
