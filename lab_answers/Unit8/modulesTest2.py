# modules Test program 2
'''Create a file called modules.py. Copy the functions in functions.py into it. 
Now create another file called modulesTest.py and import the functions to use them
in a program.'''

from modules import MenuProcess

print("Do you want to run a program that allows you to create and maintain a list?")
runIt = raw_input('Y or N?')

if runIt == 'Y' or runIt == 'y':
    MenuProcess()
else:
    print "OK. Well maybe you wanted a root beer?!! Well we don't have one here."
    print "Goodbye!"
    
