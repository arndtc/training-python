class myWaitList():
    def __init__(self):
        self.aList = []
        self.noMatch = 1

    def PrintMenu(self):
        print ""
        print "Here are your options:"
        print "1: Print list"
        print "2: Add persion to the list"
        print "3: Remove a person from the list"
        print "4: Check to see if a person is in the list"
        print "5: Exit"
        print "6: Write list to a file"
        print "7: Read list from a file"
        print "\n"

    def PrintList(self):
        print "Waiting list:\n"
        print "=============\n"
        print self.aList

    def PrintAdd(self,addName):
        self.aList.append(addName)
        print self.aList

    def RemoveName(self,removeName):
        for x in range(len(self.aList)):
            if self.aList[x] == removeName:
                del self.aList[x]
                break
        print self.aList

    def CheckName(self,checkName):
        for x in range(len(self.aList)):
            if self.aList[x] == checkName:
                print "The list does contain", self.aList[x]
                noMatch = 0;
                break
            else:
                noMatch = 1;
        if noMatch == 1:
            print "The list does not contain", checkName

    def WriteList(self):
        print "Writing File:"
        filePath = raw_input("What is the full path including file name?")
        overWrite = raw_input("Do you want to append(a) or overwrite(w)?")
        if overWrite != 'a' and overWrite != 'w':
            print "You did not choose to append or overwrite, the file will be overwritten."
            overWrite = 'w'
        fileName = open(filePath,overWrite)
        for x in self.aList:
            fileName.write(x + "\n")
        print "The wait is has been written to file:\n", filePath
        fileName.close()

    def ReadList(self):
        print "Reading File:"
        filePath = raw_input("What is the full path including file name?")
        fileName = open(filePath,'r')
        for fileLine in fileName:
            self.aList.append(fileLine[:-1])
        fileName.close()
        print "The names in the file were appended to your list."
        
    def main(self):
        self.PrintMenu()
        option = raw_input("What would you like to do? ")

        while option != '5':
            if option == '1':
                self.PrintList()
                self.PrintMenu()
                option = raw_input("What would you like to do? ")
                continue
            elif option == '2':
                addName = raw_input("Enter name you wish to add: ")
                if not addName:
                    addName = raw_input("Enter name you wish to add: ")
                else:
                    self.PrintAdd(addName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '3':
                removeName = raw_input("Please enter name you with to remove: ")
                if not removeName:
                    removeName = raw_input("Please enter name you with to remove: ")
                else:
                    self.RemoveName(removeName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '4':
                checkName = raw_input("Please enter name you with to check: ")
                if not checkName:
                    raw_input("Please enter name you with to check: ")
                else:
                    self.CheckName(checkName)
                    self.PrintMenu()
                    option = raw_input("What would you like to do? ")
                    continue
            elif option == '5':
                print "Good bye!"
                break
            elif option == '6':
                self.WriteList()
                option = raw_input("What would you like to do? ")
                continue
            elif option == '7':
                self.ReadList()
                option = raw_input("What would you like to do? ")
                continue
            else:
                print "Good bye!"
                break
